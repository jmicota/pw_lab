#include <iostream>
#include <fstream>
#include <locale>
#include <string>
#include <list>
#include <codecvt>
#include <chrono>
#include <thread>
#include <vector>
#include <future>

int grep(std::string filename, std::wstring word) {
    std::cout<<"counting for next file..\n";
    std::locale loc("pl_PL.UTF-8");
    std::wfstream file(filename);
    file.imbue(loc);
    std::wstring line;
    unsigned int count = 0;
    while (getline(file, line)) {
        for (auto pos = line.find(word,0);
             pos != std::string::npos;
             pos = line.find(word, pos+1))
            count++;
    }
    return count;
}

void do_all(std::vector<std::string> &filenames, std::wstring &word, std::promise<int> &p) {
    int count = 0;
    for (size_t i = 0; i < filenames.size(); i++) {
        count += grep(filenames[i], word);
    }
    p.set_value(count);
}

int main() {
    std::ios::sync_with_stdio(false);
    std::locale loc("pl_PL.UTF-8");
    std::wcout.imbue(loc);
    std::wcin.imbue(loc);

    std::wstring word;
    std::getline(std::wcin, word);

    std::wstring s_file_count;
    std::getline(std::wcin, s_file_count);
    int file_count = std::stoi(s_file_count);

    std::vector<std::string> filenames;
    
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;

    for (int file_num = 0; file_num < file_count; file_num++) {
        std::cout<<"parsing..";
        std::wstring w_filename;
        std::getline(std::wcin, w_filename);
        std::string s_filename = converter.to_bytes(w_filename);
        filenames.push_back(s_filename);
    }

    std::cout<<"creating promises..\n";
    std::promise<int> p1;
    std::future<int> fvalue1 = p1.get_future();
    std::promise<int> p2;
    std::future<int> fvalue2 = p2.get_future();
    std::promise<int> p3;
    std::future<int> fvalue3 = p3.get_future();
    std::cout<<"creating threads..\n";
    std::thread t1{[&filenames, &word, &p1]{ do_all(filenames, word, p1); }};
    std::thread t2{[&filenames, &word, &p2]{ do_all(filenames, word, p2); }};
    std::thread t3{[&filenames, &word, &p3]{ do_all(filenames, word, p3); }};

    int count = 0;
    std::cout<<"getting values..\n";
    count += fvalue1.get();
    count += fvalue2.get();
    count += fvalue3.get();
    std::wcout << count << std::endl;
    std::cout<<"done!\n";
    t1.join();
    t2.join();
    t3.join();
}
