#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "err.h"

#define NR_PROC 5

void split(int nr) {
    pid_t pid;
    switch (pid = fork()) {
      case -1: 
        syserr("Error in fork\n");

      case 0: /* proces potomny */
        printf("I am a child and my pid is %d\n", getpid());
        if (nr > 0) {
            split(nr - 1);
        }
        return;
    
    default: /* proces macierzysty */
        printf("I am a parent and my pid is %d\n", getpid());
        if (nr > 0 && wait(0) == -1) 
            syserr("Error in wait\n");
        return;
    } 
}

int main ()
{
  pid_t pid;
  split(NR_PROC);
  return 0;
}