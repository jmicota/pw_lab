import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.ArrayList;

// Justyna Micota 418427
public class sumyWierszami {
    private static final int WIERSZE = 10;
    private static final int KOLUMNY = 100;
    private static int aktualnyWiersz = 0;
    private static final int[] bufor = new int[KOLUMNY];
    private static final CyclicBarrier bariera = new CyclicBarrier(KOLUMNY, () -> {
        int suma = 0;
        for (int i = 0; i < KOLUMNY; i++) {
            suma += bufor[i];
        }
        System.out.println(aktualnyWiersz + " " + suma);
        aktualnyWiersz++;
    });

    private static class WatekLiczacy implements Runnable {
        private final int kolumna;

         public WatekLiczacy(int kolumna) {
             this.kolumna = kolumna;
         }

         // dla kazdego wiersza obliczamy i czekamy na resztę kolumn
        @Override
        public void run() {
             try {
                 for (int i = 0; i < WIERSZE; i++) {
                     int a = 2 * kolumna + 1;
                     bufor[kolumna] = (aktualnyWiersz + 1) * (a % 4 - 2) * a;
                     bariera.await();
                 }
             }
             catch (BrokenBarrierException | InterruptedException e) {
                 if (Thread.currentThread().isInterrupted()) {
                     System.out.println(Thread.currentThread().getName() + " został przerwany.");
                 }
             }
        }
    }

    private static void piszSumyWierszy() {

        ArrayList<Thread> listaWatkow = new ArrayList<>();
        for (int k = 0; k < KOLUMNY; ++k) {
            Thread nowyWatek = new Thread(new WatekLiczacy(k));
            listaWatkow.add(nowyWatek);
        }
        for (Thread t : listaWatkow) {
            t.start();
        }

        try {
            for (Thread t : listaWatkow) {
                t.join();
            }
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " został przerwany.");
        }
    }

    public static void main(String[] args) {
        piszSumyWierszy();
    }
}
