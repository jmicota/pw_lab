import java.util.concurrent.ConcurrentHashMap;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

// Justyna Micota 418427
public class sumyWierszami {
    private static final int WIERSZE = 10;
    private static final int KOLUMNY = 100;
    private static final ConcurrentHashMap<Integer, AtomicInteger> sumyWierszy = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Integer, AtomicInteger> liczniki = new ConcurrentHashMap<>();

    private static class WatekLiczacy implements Runnable {
        private final int kolumna;

        public WatekLiczacy(int kolumna) {
            this.kolumna = kolumna;
        }

        @Override
        public void run() {
            for (int i = 0; i < WIERSZE; i++) {
                int a = 2 * kolumna + 1;
                int wartosc = (i + 1) * (a % 4 - 2) * a;
                AtomicInteger suma = sumyWierszy.computeIfAbsent(i, (k) -> new AtomicInteger());
                suma.addAndGet(wartosc);

                AtomicInteger licznik = liczniki.computeIfAbsent(i, (k) -> new AtomicInteger());
                if (licznik.incrementAndGet() == KOLUMNY) {
                    System.out.println(i + " " + sumyWierszy.remove(i));
                    liczniki.remove(i);
                }
            }
        }
    }

    private static void piszSumyWierszy() {

        ArrayList<Thread> listaWatkow = new ArrayList<>();
        for (int k = 0; k < KOLUMNY; ++k) {
            Thread nowyWatek = new Thread(new WatekLiczacy(k));
            listaWatkow.add(nowyWatek);
        }
        for (Thread t : listaWatkow) {
            t.start();
        }

        try {
            for (Thread t : listaWatkow) {
                t.join();
            }
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " został przerwany.");
        }
    }

    public static void main(String[] args) {
        piszSumyWierszy();
    }
}
