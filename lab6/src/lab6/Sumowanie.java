package lab6;// Justyna Micota 418427

public class Sumowanie {
    private final int liczbaWatkow;
    private int nr_wiersza;
    private int suma;
    private int licznik;

    public Sumowanie(int ile_kolumn) {
        this.liczbaWatkow = ile_kolumn;
        this.suma = 0;
        this.licznik = ile_kolumn;
        this.nr_wiersza = 0;
    }

    public synchronized void dodaj(int a, int wiersz) throws InterruptedException {
        suma += a;
        --licznik;
        if (licznik == 0) {
            System.out.println(wiersz + ": " + suma);
            suma = 0;
            licznik = liczbaWatkow;
            nr_wiersza++;
            notifyAll();
        }
        else {
            while(wiersz == nr_wiersza) {
                wait();
            }
        }
    }

}
