package lab6;

import java.util.ArrayList;

// Justyna Micota 418427
class sumyWierszamiLab6 {
    private static final int WIERSZE = 10;
    private static final int KOLUMNY = 100;
    private static final Sumowanie sumowanie = new Sumowanie(KOLUMNY);

    private static class Liczacy implements Runnable {
        private final int kolumna;

        public Liczacy(int kolumna) {
            this.kolumna = kolumna;
        }

        @Override
        public void run() {
            for (int w = 0; w < WIERSZE; w++) {
                int a = 2 * kolumna + 1;
                a = (w + 1) * (a % 4 - 2) * a;
                try {
                    sumowanie.dodaj(a, w);
                } catch (InterruptedException e) {
                    System.out.println("Watek przerwany :(");
                }
            }
        }
    }

    private static void piszSumyWierszy() {
        ArrayList<Thread> listaWatkow = new ArrayList<>();
        for (int i = 0; i < KOLUMNY; i++) {
            Thread t = new Thread(new Liczacy(i));
            listaWatkow.add(t);
        }
        for (Thread t : listaWatkow) {
            t.start();
        }
        for (Thread t : listaWatkow) {
            try {
                t.join();
            } catch (InterruptedException e) {
                System.out.println("Watek glowny przerwany :o");
            }
        }
    }

    public static void main(String[] args) {
        piszSumyWierszy();
    }
}

