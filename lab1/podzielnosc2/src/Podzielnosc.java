
import java.util.ArrayList;

public class Podzielnosc {
    private static final int maxThreads = 8;
    private static volatile boolean foundDivisor = false;
    private static volatile boolean flag = false;

    private static class Assistant implements Runnable {
        private final int inc = 30;
        private int n;
        private int d;

        public Assistant(int n, int d) {
            this.n = n;
            this.d = d;
        }

        @Override
        public void run() {
            while (n > d && !foundDivisor) {
                if (n % d == 0) {
                    foundDivisor = true;
                }
                d += inc;
            }
        }
    }

    private static class testClass {
        private final Integer[] firstPrimes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
        private final Integer[] startPoints = {31, 37, 41, 43, 47, 49, 53, 59};
        private ArrayList<Thread> threadArr = new ArrayList<>();

        public boolean isPrime(int n) {
            foundDivisor = false;

            for (int i = 0; i < firstPrimes.length; i++) {
                if (n == firstPrimes[i]) {
                    return true;
                }
                else if (n % firstPrimes[i] == 0) {
                    return false;
                }
            }

            for (int i = 0; i < maxThreads; i++) {
                Thread thread = new Thread(new Assistant(n, startPoints[i]));
                threadArr.add(thread);
            }
            for (Thread thread : threadArr) {
                thread.start();
            }
            for (Thread thread : threadArr) {
                try {
                    thread.join();
//                    System.out.println("join");
                } catch(InterruptedException e) {
//                    System.out.println("interrupred exception");
                }
            }


            threadArr.clear();

            return !foundDivisor;
        }
    }

    public static void main(String[] args) {
        testClass test = new testClass();
//        Scanner scanner = new Scanner(System.in);

//        int n = scanner.nextInt();

        int count = 0;

        for (int i = 2; i <= 10000; i++) {
            if (test.isPrime(i)) {
//                System.out.println(i);
                count++;
            }
        }

        System.out.println(count);
//        System.out.println(test.isPrime(n));
    }
}