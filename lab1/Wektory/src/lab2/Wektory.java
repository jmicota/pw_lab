package lab2;

import java.util.ArrayList;
import java.util.Random;

public class Wektory {
    private static int[] globalSum;
    private static int[] globalProducts;

    private static class Vector {
        private final int length;
        private final int[] array;

        public Vector(int length) {
            this.length = length;
            array = new int[length];

            Random random = new Random();
            for (int i = 0; i < length; i++) {
                array[i] = random.nextInt(10);
            }
        }

        public int getCell(int i) {
            return array[i];
        }

        public int getLength() {
            return length;
        }
    }

    static void countSum(Vector array1, Vector array2, int start, int end) {

        for (int i = start; i < end; i++) {
            globalSum[i] = array1.getCell(i) + array2.getCell(i);
        }
    }

    static void countProduct(Vector array1, Vector array2, int start, int end) {

        int product = 0;

        for (int i = start; i < end; i++) {
            product += array1.getCell(i) * array2.getCell(i);
        }

        globalProducts[start / 10] = product;
    }

    private static class VectorOperations implements Runnable {
        private final Vector array1;
        private final Vector array2;
        private final int start;
        private final int end;

        private VectorOperations(Vector array1, Vector array2, int start, int end) {
            this.array1 = array1;
            this.array2 = array2;
            this.start = start;
            this.end = end;
        }

        @Override
        public void run() {
            countSum(array1, array2, start, end);
            countProduct(array1, array2, start, end);
        }
    }

    private static class MainProcess {
        private final int vectorLength;

        private MainProcess(int vectorLength) {
            this.vectorLength = vectorLength;
        }

        public void process() {
            Vector vector1 = new Vector(vectorLength);
            Vector vector2 = new Vector(vectorLength);

            printVectors(vector1, vector2);

            int howManyThreads = vector1.length / 10;
            int bonusThreadLength = vector1.length % 10;
            if (bonusThreadLength > 0) {
                howManyThreads++;
            }

            globalSum = new int[vector1.length];
            globalProducts = new int[howManyThreads];
            ArrayList<Thread> threadArr = new ArrayList<>();

            int i;
            for (i = 0; i < howManyThreads - 1; i++) {
                Thread newThread = new Thread(new VectorOperations(vector1,
                        vector2, i * 10, i * 10 + 10));
                threadArr.add(newThread);
            }
            Thread lastThread = new Thread(new VectorOperations(vector1,
                    vector2, i * 10, vectorLength));
            threadArr.add(lastThread);

            for (Thread thread : threadArr) {
                thread.start();
            }
            for (Thread thread : threadArr) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    System.out.println("interrupted exception :)");
                }
            }

            threadArr.clear();
        }
    }

    private static void printVectors(Vector vector1, Vector vector2) {

        System.out.print("\nVector1: ");
        for(int i = 0; i < vector1.getLength(); i++) {
            System.out.print("[" + vector1.getCell(i) + "]");
        }
        System.out.print("\nVector2: ");
        for(int i = 0; i < vector2.getLength(); i++) {
            System.out.print("[" + vector2.getCell(i) + "]");
        }
        System.out.print("\n");
    }

    private static void printSolution() {

        System.out.print("SUM: ");
        for (int i : globalSum) {
            System.out.print("[" + i + "]");
        }
        System.out.print("\nPRODUCT (sum of products counted by each thread): ");
        int sumOfProducts = 0;
        for (int i = 0; i < globalProducts.length - 1; i++) {
            System.out.print(globalProducts[i] + " + ");
            sumOfProducts += globalProducts[i];
        }
        System.out.print(globalProducts[globalProducts.length - 1] + " = ");
        sumOfProducts += globalProducts[globalProducts.length - 1];
        System.out.print(sumOfProducts + "\n");
    }

    public static void main(String[] args) {

        Random random = new Random();
        int randomLength = random.nextInt(50);
        MainProcess processingClass = new MainProcess(randomLength);
        processingClass.process();
        printSolution();
    }



}