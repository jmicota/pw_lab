
public class Podzielnosc {
    static int[] wartosciPoczatkowe = {31, 37, 41, 43, 47, 49, 53, 59};
    static boolean[] czyZnaleziony = {false,false,false,false,false,false,false,false};

    private static class SprawdzajacyPodzielnosc implements Runnable {
        private final int liczba;
        private final int ktory;

        public SprawdzajacyPodzielnosc(int liczba, int ktory) {
            this.liczba = liczba;
            this.ktory = ktory;
        }

        @Override
        public void run() {
            if (ktory < 6) {
                Thread nowyWatek = new Thread(new SprawdzajacyPodzielnosc(liczba,  ktory + 1));
                nowyWatek.start();
                while (nowyWatek.isAlive()) {
                    // nic
                }
            }

            int wartosc = wartosciPoczatkowe[ktory];
            czyZnaleziony[ktory] = false;
            while (wartosc * wartosc <= liczba && czyBrakDzielnikow()) {

                if (liczba % wartosc == 0) {
                    czyZnaleziony[ktory] = true;
                }
                else wartosc += 30;
            }
        }
    }

    static boolean sprawdzeniePoczatkowe(int a) {
        int[] dzielniki = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
        for (int value : dzielniki) {
            if (a % value == 0 && a != value) {
                return false;
            }
        }
        return true;
    }

    static boolean czyBrakDzielnikow() {
        for (int i = 0; i < 8; i++) {
            if (czyZnaleziony[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean czyPierwsza(int n) {
        boolean sprPoczatkowe = false;
        sprPoczatkowe = sprawdzeniePoczatkowe(n);

        if (sprPoczatkowe) {
            Thread watekStartowy = new Thread(new SprawdzajacyPodzielnosc(n, 0));
            watekStartowy.start();
            while (watekStartowy.isAlive()) {
                // nic
            }
        }
        else return sprPoczatkowe;

        return czyBrakDzielnikow();
    }

    public static void main(String[] args) {
        int ilePierwszych = 0;

        for (int i = 2; i <= 10000; i++) {
            if (czyPierwsza(i))
                ilePierwszych += 1;
        }

        System.out.println("Jest " + ilePierwszych + " liczb pierwszych między 2 a 10000.");
    }
}
