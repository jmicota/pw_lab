import java.util.concurrent.TimeUnit;

public class Wiele {

    private static class Piszący implements Runnable {
        private int nr;

        public Piszący(int nr) {
            this.nr = nr;
        }

        @Override
        public void run() {
            if (nr > 0) {
                Thread rek = new Thread(new Wiele.Piszący(nr - 1));
                rek.start();

                //while (rek.isAlive()) {
                    // empty while
                //}

                if (nr%2 == 0) {
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.print(nr);
            }

        }

    }

    public static void main(String[] args) {
        Thread pierwszy = new Thread(new Wiele.Piszący(10));
        pierwszy.start();

        //while (pierwszy.isAlive()) {
            // empty while
        //}
        //System.out.println("\nKoniec");
    }
}
