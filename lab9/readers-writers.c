#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include "err.h"

#define READERS 3
#define WRITERS 2
#define NAP 2
#define BSIZE 32

struct readwrite {
  pthread_mutex_t lock;         
  pthread_cond_t readers;      
  pthread_cond_t writers;    
  int rcount, wcount, rwait, wwait;
  bool written;
};

struct readwrite library;
char book[BSIZE];
int working = 1;

/* Initialize a buffer */
void init(struct readwrite *rw) {
  int err;

  if ((err = pthread_mutex_init(&rw->lock, 0) != 0))
    syserr (err, "mutex init failed");
  if ((err = pthread_cond_init(&rw->readers, 0) != 0))
    syserr (err, "cond init 1 failed");
  if ((err = pthread_cond_init(&rw->writers, 0) != 0))
    syserr (err, "cond init 2 failed");
  rw->rcount = 0;
  rw->wcount = 0;
  rw->rwait = 0;
  rw->wwait = 0;
  rw->written = false;
}

/* Destroy the buffer */
void destroy(struct readwrite *rw) {
  int err;

  if ((err = pthread_mutex_destroy(&rw->lock) != 0))
    syserr (err, "mutex destroy failed");
  if ((err = pthread_cond_destroy(&rw->readers) != 0))
    syserr (err, "cond destroy 1 failed");
  if ((err = pthread_cond_destroy(&rw->writers) != 0))
    syserr (err, "cond destroy 2 failed");
}

void my_read(struct readwrite *lib)
{
  int err;
  while (working) {
    
    if ((err = pthread_mutex_lock(&lib->lock)) != 0)  
      syserr (err, "lock failed");

    /* wait if there is anyone in library at the moment */
    if (lib->rcount + lib->wcount > 0 || lib->written == false) {
      lib->rwait++;
      if ((err = pthread_cond_wait(&lib->readers, &lib->lock)) != 0)
        syserr (err, "cond wait failed");
      lib->rwait--;
    }
    
    /* if someone woke me up, then i can read */
    lib->rcount++;
    printf("reader read: %s\n", book); /* reading */
    lib->rcount--;

    if (lib->rcount == 0) {
      /* i was a last reader */
      /* first writer, if not then readers */
      if (lib->wwait > 0) {
        if ((err = pthread_cond_signal(&lib->writers)) != 0)
          syserr (err, "cond signal failed");
      }
      else if (lib->rwait > 0) {
        int to_let_in = lib->rwait;
        for (int i = 0; i < to_let_in; i++) {
          if ((err = pthread_cond_signal(&lib->readers)) != 0)
            syserr (err, "cond signal failed");
        }
      }
    }

    if ((err = pthread_mutex_unlock(&lib->lock)) != 0)
      syserr (err, "unlock failed");
  }
}

void my_write(struct readwrite *lib)
{  
  int l, err;
  while (working) {

    if ((err = pthread_mutex_lock(&lib->lock)) != 0)  
      syserr (err, "lock failed");
    
    /* wait if there is anyone in library at the moment */
    if (lib->rcount + lib->wcount > 0) {
      lib->wwait++;
      if ((err = pthread_cond_wait(&lib->writers, &lib->lock)) != 0)
        syserr (err, "cond wait failed");
      lib->wwait--;
    }

    /* if someone woke me up, then i can write */
    lib->wcount++;
    l = rand()%10;
    snprintf(book, BSIZE, "6 reps of %d %d %d %d %d %d", l, l, l, l, l, l);
    if (!(lib->written)) lib->written = true;
    lib->wcount--;

    /* first readers, if not then writer */
    if (lib->rwait > 0) {
      int to_let_in = lib->rwait;
      for (int i = 0; i < to_let_in; i++) {
        if ((err = pthread_cond_signal(&lib->readers)) != 0)
          syserr (err, "cond signal failed");
      }
    }
    else if (lib->wwait > 0) {
      if ((err = pthread_cond_signal(&lib->writers)) != 0)
        syserr (err, "cond signal failed");
    }

    if ((err = pthread_mutex_unlock(&lib->lock)) != 0)
      syserr (err, "unlock failed");
  }
}

struct readwrite library;

void *reader() {
  my_read(&library);
  return 0;
}

void *writer() {
  my_write(&library);
  return 0;
}

int main() {
  pthread_t th[READERS+WRITERS];
  pthread_attr_t attr;
  int i, err;
  void *retval;

  srand((unsigned)time(0));
  
  init(&library);
  if ((err = pthread_attr_init (&attr)) != 0)
    syserr (err, "attr_init failed");
  if ((err = pthread_attr_setdetachstate (&attr,PTHREAD_CREATE_JOINABLE)) != 0)
    syserr (err, "attr_setdetachstate failed");

  for (i = 0; i < READERS + WRITERS; i++) {
    if (i < READERS) {
      if ((err = pthread_create(&th[i], &attr, reader, 0)) != 0)
	      syserr (err, "create failed");
    } else 
      if ((err = pthread_create(&th[i], &attr, writer, 0)) != 0)
	      syserr (err, "create failed");
  }
  
  sleep(NAP);
  working = 0;

  for (i = 0; i < READERS + WRITERS; i++) {
    if ((err = pthread_join(th[i], &retval)) != 0)
    syserr (err, "join failed");
  }
  
  if ((err = pthread_attr_destroy (&attr)) != 0)
    syserr (err, "cond destroy failed");
  destroy(&library);
  return 0;
}
