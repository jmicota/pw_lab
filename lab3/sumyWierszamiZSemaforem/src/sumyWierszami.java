import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

// Justyna Micota 418427
public class sumyWierszami {
    private static final int WIERSZE = 10;
    private static final int KOLUMNY = 100;
    private static final ConcurrentHashMap<Integer, AtomicInteger> sumyWierszy = new ConcurrentHashMap<>();
    private static final ArrayList<Integer> liczniki = new ArrayList<>();
    private static final Semaphore mutex = new Semaphore(1);

    private static class WatekLiczacy implements Runnable {
        private final int kolumna;

        public WatekLiczacy(int kolumna) {
            this.kolumna = kolumna;
        }

        @Override
        public void run() {
            for (int i = 0; i < WIERSZE; i++) {
                int a = 2 * kolumna + 1;
                int wartosc = (i + 1) * (a % 4 - 2) * a;
                AtomicInteger suma = sumyWierszy.computeIfAbsent(i, (k) -> new AtomicInteger());
                suma.addAndGet(wartosc);

                // każdy wątek zmienia numer wiersza w którym obecnie się
                // znajduje za pomocą sekcji krytycznej
                // jeśli poprzez tą zmianę zmienił się minimalny przetrzymywany nr
                // wiersza dla wątków to wypisywany jest wiersz
                try {
                    mutex.acquire();
                    int minimum = Collections.min(liczniki);
                    liczniki.set(kolumna, liczniki.get(kolumna) + 1);
                    int noweMinimum = Collections.min(liczniki);
                    if (noweMinimum != minimum) {
                        System.out.println(i + " " + sumyWierszy.remove(i));
                    }
                    mutex.release();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void piszSumyWierszy() {

        ArrayList<Thread> listaWatkow = new ArrayList<>();
        for (int k = 0; k < KOLUMNY; ++k) {
            Thread nowyWatek = new Thread(new WatekLiczacy(k));
            listaWatkow.add(nowyWatek);
            liczniki.add(0); // dla wątku kolumny k dodajemy że jest w wierszu zerowym
        }
        for (Thread t : listaWatkow) {
            t.start();
        }

        try {
            for (Thread t : listaWatkow) {
                t.join();
            }
        } catch (InterruptedException e) {
            System.out.println(Thread.currentThread().getName() + " został przerwany.");
        }
    }

    public static void main(String[] args) {
        piszSumyWierszy();
    }
}

