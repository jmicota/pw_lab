#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "err.h"

#define N           20
#define K           5
#define BUFFER_SIZE 1000
#define EXTRA_NAP   5
#define SIG         (SIGRTMIN + 1)

int pipe_to_write, count_sig = 0, count_children = 0;

void catch(int sig) { 
  
  count_sig++;
  printf("Dostałem sygnał %d.\n", count_sig); 
  
  if (count_sig == N) {
    // let N in
    for (int i = 0; i < N; i++) {
      if (write (pipe_to_write, "c", strlen("c")) != strlen("c"))
        syserr("Error in parent write\n");
      count_children++;
    }
  }
  else if (count_sig > N) {
    // let in
    if (write (pipe_to_write, "c", strlen("c")) != strlen("c"))
      syserr("Error in parent write\n");
  }
}

int main ()
{
  struct sigaction action;
  sigset_t block_mask;
  int pipe_dsc[2];
  pid_t parent_pid, child_pid[N];
  int i = 0;
  char buffer[BUFFER_SIZE];

  srand((unsigned) time(0));
  parent_pid = getpid();

  sigemptyset(&block_mask);
  sigaddset(&block_mask, SIG); 

  action.sa_handler = catch;
  action.sa_mask = block_mask;
  action.sa_flags = 0;

  if (sigaction (SIG, &action, 0) == -1)
    syserr("Error in sigaction\n");

  if (pipe(pipe_dsc) == -1)
    syserr("Error in opening pipe_dsc\n");
  pipe_to_write = pipe_dsc[1];

  // tworzę N dzieci
  for(i = 0; i < N; ++i) { 
    int sleep_time = rand() % 10 + 1;
    
    switch (child_pid[i] = fork()) {  
      
      case -1:
        syserr("Error in fork\n");
      
      case 0:
        printf("Dziecko %d (%d): Zaczynam\n", i+1, getpid());
        sleep(sleep_time);
        
        // sygnalizuję rodzicowi
        if (kill(parent_pid, SIG) == -1)
          syserr("Error in kill\n");

        if (close(pipe_dsc[1]) == -1)
          syserr("Error in child close pipe_dsc[1]\n");

        // czekam na wiadomość
        if (read(pipe_dsc[0], buffer, 1) == -1)
          syserr("Error in child read\n");

        if (close(pipe_dsc[0]) == -1)
          syserr("Error in child close pipe_dsc[0]\n");

        printf("Dziecko %d (%d): Kończę\n", i+1, getpid());
        return 0;
      
      default:
        #ifdef D
          printf("Parent %d\n", i);
        #endif
        break;
    }
  }

  // parent
  while (count_children < N) { 
    sleep(1); 
  }

  printf("Rodzic: Kończę\n");
  return 0;  
}

