import java.util.concurrent.*;

// Justyna Micota 418427
public class sumyWierszami {
    private static final int WIERSZE = 10;
    private static final int KOLUMNY = 100;
    private static final Semaphore mutex = new Semaphore(1);
    private static int suma = 0;

    private static class Liczacy implements Callable<Integer> {
        private final int kolumna;
        private final int wiersz;

        public Liczacy(int wiersz, int kolumna) {
            this.kolumna = kolumna;
            this.wiersz = wiersz;
        }

        @Override
        public Integer call() {
                int a = 2 * kolumna + 1;
                return (wiersz + 1) * (a % 4 - 2) * a;
        }
    }

    private static class Piszacy implements Runnable {
        private final int wartosc;
        private final int wiersz;

        public Piszacy(int wiersz, int wartosc) {
            this.wartosc = wartosc;
            this.wiersz = wiersz;
        }

        @Override
        public void run() {
            System.out.println(wiersz + ": " + wartosc);
        }
    }

    private static void piszSumyWierszy() {

        ExecutorService pulaLiczacych = Executors.newFixedThreadPool(4);
        ExecutorService pulaPiszacych = Executors.newFixedThreadPool(4);
        try {
            for (int w = 0; w < WIERSZE; w++) {
                suma = 0;
                for (int k = 0; k < KOLUMNY; k++) {
                    Callable<Integer> praca = new Liczacy(w, k);
                    Future<Integer> wartosc = pulaLiczacych.submit(praca);
                    try {
                        mutex.acquire();
                        suma += wartosc.get();
                        mutex.release();
                    } catch (InterruptedException | ExecutionException e) {
                        System.out.println("Watek przerwany lub execution error");
                    }
                }
                pulaPiszacych.submit(new Piszacy(w, suma));
            }
        } finally {
            pulaLiczacych.shutdown();
            pulaPiszacych.shutdown();
        }

    }

    public static void main(String[] args) {
        piszSumyWierszy();
    }
}

