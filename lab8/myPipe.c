#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "err.h"

char message[] = "abc";

int main(int argc, char* argv[])
{
  for (int i = 1; i < argc; ++i) {
    
    int pipe_dsc[2];
    if (pipe(pipe_dsc) == -1) syserr("Error in pipe\n");
    
    switch (fork()) {
      case -1:
        syserr("Error in fork\n");
      
      case 0:
        if (close(0) == -1)
          syserr("Error in close(0) in loop %d\n", i);
        if (dup(pipe_dsc[0]) != 0)
          syserr("Error in duplicating in loop %d", i);
        
      default:
        if (close(pipe_dsc[0]) == -1) 
          syserr("Error in closing input in loop %d\n", i);
        
        if (close(pipe_dsc[1]) == -1) 
          syserr("Error in closing output in loop %d\n", i);

        if (dup(pipe_dsc[1]) != 0)
          syserr("Error in duplicating in loop %d\n", i);
        
        char *command_args[2] = {argv[i], NULL};
        execv(command_args[0], command_args);

        if (close(pipe_dsc[1] == -1))
          syserr("Error in closing pipe_dsc[1] in loop %d", i);
        if (wait(0) == -1)
          syserr("Error in wait\n");
        
        exit(0);
    }
  }
}
